create table company (id number primary key, name varchar(255));
create table status (id number primary key, name varchar(255));
create table contact (id number primary key, first_name varchar(255), last_name varchar(255),
    company_id number, status_id number, email varchar(255),
    foreign key (company_id) references company(id),
    foreign key (status_id) references status(id)
);

insert into company (id, name) values (1, 'Compañia 1');
insert into company (id, name) values (2, 'Compañia 2');

insert into status values (1, 'Activo');
insert into status values (2, 'Inactivo');

insert into contact values (1, 'Gorka', 'Alvarez', 1, 1, 'email@email.com');