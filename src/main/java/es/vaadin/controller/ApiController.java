package es.vaadin.controller;

import com.google.gson.Gson;
import es.vaadin.dto.User;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;

@RestController
public class ApiController {


    @GetMapping(value = "/users", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> getUsers(){

        User user = new User();
        user.setId(1);
        user.setName("Gorka");

        ArrayList<User> usersList = new ArrayList<>();
        usersList.add(user);

        return ResponseEntity.status(HttpStatus.OK).body(new Gson().toJson(usersList));

    }
}
