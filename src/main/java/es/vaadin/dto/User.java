package es.vaadin.dto;

import lombok.Data;

@Data
public class User {

    Integer id;

    String name;

}
