package es.vaadin.views;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.html.H1;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.notification.NotificationVariant;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;

@Route("view")
@PageTitle("Titulo de la pagina")
public class MainView extends VerticalLayout {

    public MainView(){

        H1 heading = new H1("Vista principal");
        add(heading);

        Button button = new Button();
        button.setText("Boton");

        this.setHorizontalComponentAlignment(Alignment.CENTER, heading);
        this.setHorizontalComponentAlignment(Alignment.CENTER, button);

        button.addClickListener(buttonClickEvent -> {
            Notification notification = Notification.show("Todo Gucci", 1000, Notification.Position.MIDDLE);
            notification.addThemeVariants(NotificationVariant.LUMO_PRIMARY);
        });

        add(button);
    }
}
