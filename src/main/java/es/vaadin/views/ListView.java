package es.vaadin.views;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.html.H2;
import com.vaadin.flow.component.html.Paragraph;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;

@PageTitle("List")
@Route("")
public class ListView extends VerticalLayout {

    public ListView() {
        this.add(new H2("Texto de titulo"));
        this.add(new Paragraph("Un parrafo para meter cualquier chorrada"));

        TextField name = new TextField("Name", "Gorka");
        Button button = new Button("Click me!");
        //this.add(button, name);

        HorizontalLayout hl = new HorizontalLayout(name, button);
        hl.setDefaultVerticalComponentAlignment(Alignment.BASELINE);

        button.addClickListener(click -> {
            Notification.show("Hello, " + name.getValue());
        });



        this.add(hl);




        //Estos todos sirven para centrar las cosas
        this.setSizeFull();
        this.setJustifyContentMode(JustifyContentMode.CENTER);
        this.setDefaultHorizontalComponentAlignment(Alignment.CENTER);
        getStyle().set("text-align", "center");
    }
}
