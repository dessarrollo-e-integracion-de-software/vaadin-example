package es.vaadin.views.contacts;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.value.ValueChangeMode;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import es.vaadin.data.entity.Contact;
import es.vaadin.data.service.CrmService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.GenericApplicationContext;

import javax.annotation.PostConstruct;

@PageTitle("Contacts | Vaadin CRM")
@Route("contacts")
@org.springframework.stereotype.Component
public class ContactListView extends VerticalLayout {

    Grid<Contact> grid = new Grid<>(Contact.class);
    TextField filterText = new TextField();
    ContactForm form;

    CrmService crmService;

    public ContactListView(CrmService crmService) {
        this.crmService = crmService;
        addClassName("list-view");

        //Hace que la vista ocupe toda la pantalla del pc
        setSizeFull();

        configureGrid();
        configureForm();

        this.add(
                getToolbar(),
                getContent()
        );

        updateList();
        closeEditor();


    }

    private void closeEditor() {
        form.setContact(null);
        form.setVisible(false);
        removeClassName("editing");
    }

    private void updateList() {

        grid.setItems(crmService.findAllContacts(filterText.getValue()));
    }

    private Component getContent() {
        HorizontalLayout content = new HorizontalLayout(grid, form);
        content.setFlexGrow(2, grid);
        content.setFlexGrow(1, form);
        content.addClassName("content");
        content.setSizeFull();

        return content;
    }

    private void configureForm() {
        form = new ContactForm(crmService.findAllCompanies(), crmService.findAllStatuses());
        form.setWidth("25em");

    }

    private Component getToolbar(){
        filterText.setPlaceholder("Filter by name...");
        filterText.setClearButtonVisible(true);
        //Esto hace que solamente haga el filtre una vez termine de escribir
        //Si no lo pongo nada hara un filtro por cada tecla que de
        filterText.setValueChangeMode(ValueChangeMode.LAZY);
        filterText.addValueChangeListener(e -> updateList());

        Button button = new Button("Add Contact");
        button.addClickListener(e -> addContact());

        HorizontalLayout toolbar = new HorizontalLayout(filterText, button);
        toolbar.addClassName("toolbar");

        return toolbar;

    }

    private void addContact() {
        grid.asSingleSelect().clear();
        editContact(new Contact());
    }

    /**
     * Configura el grid que mostrara los datos
     * Basicamente lo primero que hago es darle un nombre al grid.
     * Luego añado las columnas para los campos del contacto.
     * Luego para cada contacto imprimire una columna con su estado y su compañia
     * Por ultimo hago que cada columna se autoconfigure el tamaño.
     */
    private void configureGrid(){
        grid.addClassName("contact-grid");
        grid.setSizeFull();
        //Tienen que tener el mismo nombre que el atributo en la clase Contact
        grid.setColumns("firstName", "lastName", "email");
        grid.addColumn(contact -> contact.getStatus().getName()).setHeader("Status");
        grid.addColumn(contact -> contact.getCompany().getName()).setHeader("Company");

        grid.getColumns().forEach(col -> col.setAutoWidth(true));

        grid.asSingleSelect().addValueChangeListener(e -> editContact(e.getValue()));

    }

    private void editContact(Contact contact) {
        if(contact == null){
            closeEditor();
        }else{
            form.setContact(contact);
            form.setVisible(true);
            addClassName("editing");
        }
    }
}
