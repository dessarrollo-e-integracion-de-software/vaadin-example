package es.vaadin.data.entity;

import es.vaadin.data.AbstractEntity;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Table;


@Entity
@Data
@Getter
@Setter
@Table(name = "status")
public class Status extends AbstractEntity {

    private String name;

}
