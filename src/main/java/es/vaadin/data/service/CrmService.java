package es.vaadin.data.service;

import es.vaadin.data.entity.Company;
import es.vaadin.data.entity.Contact;
import es.vaadin.data.entity.Status;
import es.vaadin.data.repository.CompanyRepository;
import es.vaadin.data.repository.ContactRepository;
import es.vaadin.data.repository.StatusRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CrmService {

    @Autowired
    ContactRepository contactRepository;

    @Autowired
    CompanyRepository companyRepository;

    @Autowired
    StatusRepository statusRepository;


    public List<Contact> findAllContacts(String filterText){
        if(filterText == null || filterText.isEmpty()){
            return contactRepository.findAll();
        }else{
            return contactRepository.search(filterText);
        }
    }

    public long countContacts(){
        return  contactRepository.count();
    }

    public void deleteContact(Contact contact){
        contactRepository.delete(contact);
    }

    public Contact saveContact(Contact contact){
        if(contact == null){
            System.err.println("Contact is null");
            return null;
        }

        return contactRepository.save(contact);
    }

    public List<Company> findAllCompanies(){
        return companyRepository.findAll();
    }

    public List<Status> findAllStatuses(){
        return statusRepository.findAll();
    }



}
