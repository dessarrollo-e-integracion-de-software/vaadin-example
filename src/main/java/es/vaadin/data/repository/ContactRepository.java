package es.vaadin.data.repository;

import es.vaadin.data.entity.Contact;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ContactRepository extends JpaRepository<Contact, Integer> {

    @Query(value = "select * from contact where lower(first_name) like lower( :filterText) " +
                    "or lower(last_name) like lower( :filterText)", nativeQuery = true)
    List<Contact> search(@Param("filterText") String filterText);
}
